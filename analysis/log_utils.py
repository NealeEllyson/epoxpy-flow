import cme_utils
from cme_utils.analyze import autocorr
import numpy as np


def _get_decorrelation_time(prop_values,
                             time_steps):
    t = time_steps - time_steps[0]
    dt = t[1] - t[0]
    acorr = autocorr.autocorr1D(prop_values)
    for acorr_i in range(len(acorr)):
        if acorr[acorr_i]<0:
            break
    lags = [i*dt for i in range(len(acorr))]

    decorrelation_time = int(lags[acorr_i])
    if decorrelation_time == 0:
        decorrelation_time = 1
    decorrelation_stride = int(decorrelation_time/dt)
    nsamples = (int(t[-1])-t[0])/decorrelation_time
    temps = "There are %.5e steps, (" % t[-1]
    temps = temps + "%d" % int(t[-1])
    temps = temps + " frames)\n"
    temps = temps + "You can start sampling at t=%.5e" % t[0]
    temps = temps + " (frame %d)" % int(t[0] )
    temps = temps + " for %d samples\n" % nsamples
    temps = temps + "Because the autocorrelation time is %.5e" % lags[acorr_i]
    temps = temps + " (%d frames)\n" % int(lags[acorr_i])
    print(temps)
    return decorrelation_time, decorrelation_stride

def get_mean_and_std(job, prop):
    if job.isfile('out.log'):
        log_path = job.fn('out.log')
        data = np.genfromtxt(log_path, names=True)
        prop_values = data[prop]
        time_steps = data['timestep']
        PE = data['potential_energy']
        prop_std = np.std(prop_values)
        print(prop_values,prop_std)
        if prop_std < 1e-10:# this limit is kind of arbitary, but avoids error from numpy due to zero standard deviation in prop_values
            print('{} for {} has a standard deviation of {}'.format(prop,job,prop_std))
            mean = np.mean(prop_values)
            std = 0.0
        else:
            start_i, start_t = autocorr.find_equilibrated_window(time_steps,PE)#prop_values)
            if start_i < len(time_steps):
                _, decorrelation_stride = _get_decorrelation_time(prop_values[start_i:],time_steps[start_i:])
                independent_vals_i = np.arange(start_i, len(prop_values)-1, decorrelation_stride)
                independent_vals = prop_values[independent_vals_i]
                #print(independent_vals)
                mean=np.mean(independent_vals)
                std=np.std(independent_vals)
            else:
                print('the {} values given have not reached equilibrium.'.format(prop))
                mean = None
                std = None
    else:
        print('could not find log file for {}'.format(job))
        mean=None
        std=None
    #print(mean)
    return mean, std

