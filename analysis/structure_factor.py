import gsd
import gsd.fl
import gsd.hoomd
import math
from scipy.signal import argrelextrema as argex
import hoomd.deprecated
import hoomd.dump
import hoomd
import numpy as np
import os
from cme_utils.analyze import diffractometer
from scipy.signal import argrelextrema

def diffract_last_frame(job,typeId=2,n_views=20,grid_size=512):
    f = gsd.fl.GSDFile(job.fn('data.gsd'), 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    n_frames = len(t)
    end_frame = n_frames-1
    snapshot = t[end_frame]
    sim_box = snapshot.configuration.box
    box_dim=(sim_box[0], sim_box[1], sim_box[2])
    box_dim = np.array(box_dim)
    print('box_dim',box_dim)
    l_pos = snapshot.particles.position
    pos = l_pos[np.where(snapshot.particles.typeid == typeId)]
    diffract_dir = job.workspace()+'/diffract_type_{}'.format(typeId)
    if not os.path.exists(diffract_dir):
            os.makedirs(diffract_dir)
    D = diffractometer.diffractometer(working_dir=diffract_dir)
    D.set(grid_size=grid_size,  peak_width=1, zoom=8, n_views=n_views, length_scale=1.0,bot=1e-5,top=1)
    D.load(pos,box_dim)
    D.prep_matrices()
    D.average()

def diffract_frame(job,diff_dir,frame,typeId=2,n_views=20,grid_size=512):
    f = gsd.fl.GSDFile(job.fn('data.gsd'), 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    snapshot = t[frame]
    sim_box = snapshot.configuration.box
    box_dim=(sim_box[0], sim_box[1], sim_box[2])
    box_dim = np.array(box_dim)
    print('box_dim',box_dim)
    l_pos = snapshot.particles.position
    pos = l_pos[np.where(snapshot.particles.typeid == typeId)]
    if not os.path.exists(diff_dir):
        os.makedirs(diff_dir)
    D = diffractometer.diffractometer(working_dir=diff_dir)
    D.set(grid_size=grid_size,peak_width=1,zoom=8,n_views=n_views,
          length_scale=1.0,bot=1e-5,top=1)
    D.load(pos,box_dim)
    D.prep_matrices()
    D.average()

def save_structure_factor(job, lastFrame=True):
    if lastFrame:
        diffract_last_frame(job,typeId=2)

def first_peak(job,q,intensities,force=False,min_pos_slope=0.1):
    dI = np.gradient(intensities,edge_order=1)
    dq = np.gradient(q,edge_order=1)
    dIdqs = dI/dq
    detected_neg_to_pos_trans = False
    max_pos_slope=max(dIdqs)
    first_peak_i = first_peak_q = None
    for i,dIdq in enumerate(dIdqs):
        if i != 0:
            if (dIdqs[i-1]<0 and dIdqs[i] >0):
                detected_neg_to_pos_trans = True
            if (dIdqs[i-1]>0 and dIdqs[i] <0) and\
               (detected_neg_to_pos_trans == True) and\
               (max_pos_slope > min_pos_slope):
                q_at_zero_dIdq = np.interp(0,dIdqs[i-1:i],q[i-1:i])
                I_at_interp_q = np.interp(q_at_zero_dIdq,q,intensities)
                x2 = q_at_zero_dIdq
                y2 = I_at_interp_q
                first_peak_q = x2
                first_peak_i = y2
                break
    return first_peak_q,first_peak_i,dIdqs

def detect_first_peak(job,q,intensities,force=False,min_pos_slope=0.1):
    dI = np.gradient(intensities,edge_order=1)
    dq = np.gradient(q,edge_order=1)
    dIdqs = dI/dq
    detected_neg_to_pos_trans = False
    max_pos_slope=max(dIdqs)
    first_peak_i = first_peak_q = None
    for i,dIdq in enumerate(dIdqs):
        if i != 0:
            if (dIdqs[i-1]<0 and dIdqs[i] >0):
                detected_neg_to_pos_trans = True
            if (dIdqs[i-1]>0 and dIdqs[i] <0) and\
               (detected_neg_to_pos_trans == True) and\
               (max_pos_slope > min_pos_slope):
                q_at_zero_dIdq = np.interp(0,dIdqs[i-1:i],q[i-1:i])
                I_at_interp_q = np.interp(q_at_zero_dIdq,q,intensities)
                x2 = q_at_zero_dIdq
                y2 = I_at_interp_q
                first_peak_q = x2
                first_peak_i = y2
                break
    return first_peak_q,first_peak_i,dIdqs

def detect_plateau(job,q,intensities,force=False,max_slope=0.1):
    dI = np.gradient(intensities,edge_order=1)
    dq = np.gradient(q,edge_order=1)
    dIdqs = dI/dq
    detected_neg_to_pos_trans = False
    max_pos_slope=max(dIdqs)
    first_peak_i = first_peak_q = None
    lmins_idxs = argrelextrema(dIdqs,np.less)[0]
    lmaxs_idxs = argrelextrema(dIdqs,np.greater)[0]
    first_max_idx = lmaxs_idxs[0]
    mid_idx = first_max_idx
    plateau_mid_I = intensities[mid_idx]
    plateau_mid_q = q[mid_idx]
    return plateau_mid_q,plateau_mid_I,dIdqs

def save_peaks_and_time_evo(job,typeId=2,grid_size=512,n_views=40):
    f = gsd.fl.GSDFile(job.fn('data.gsd'), 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    print('number of frames:',len(t))
    n_frames = len(t)-1
    n_diffracted_frames=30
    fraction_of_frames_to_diffract=n_diffracted_frames/n_frames
    n_samples = int(n_frames*fraction_of_frames_to_diffract)
    frames = np.asarray(np.linspace(1,n_frames,n_samples),dtype=int)
    qs_list = []
    times_list = []
    print('diffracting frames:',frames)
    for i,frame in enumerate(frames):
        print('diffracting frame {}({}/{})'.format(frame,i+1,len(frames)))
        diffract_dir =job.workspace()+'/diffract_type_{}_n_views_{}_grid_size_{}_frame_{}'.format(typeId,
                                                                                                     n_views,
                                                                                                     grid_size,
                                                                                                     frame)
        #print(diffract_dir)
        if job.isfile('{}/asq.txt'.format(diffract_dir))==False:
            diffract_frame(job,
                           diff_dir=diffract_dir,
                           frame=int(frame),
                           typeId=typeId,
                           n_views=n_views,
                           grid_size=grid_size)
        time = round(frame*job.sp.md_dt*job.sp.dcd_write)
        data=np.genfromtxt(job.fn('{}/asq.txt'.format(diffract_dir)))
        qs = data[:,0]
        Is = data[:,1]
        first_peak_q,first_peak_i,dIdqs = detect_first_peak(job,qs,Is,min_pos_slope=0.2)
        if first_peak_q is None:
            first_peak_q,first_peak_i,dIdqs = detect_plateau(job,qs,Is,max_slope=0.2)
        qs_list.append(first_peak_q)
        times_list.append(time)
    with open(job.fn('qm.txt'), 'w') as file:
        for x,y in zip(times_list,qs_list):
            file.write('{}\t{}\n'.format(x,y))


def get_all_maximas(qs, intensities, box_length, half_box_fudge_factor=0.6):
    """
    Returns all the maximas in the structure factor
    :param qs: wave vectors from scattering
    :param intensities: intensity at wave vector values (q)
    :param box_length: box length
    :param half_box_fudge_factor: Sometimes the first peak we want to detect is close to the larger than the 
    half box length because it. Here the half box length is 0.6 instead of 0.5 to detect some periodic distances that 
    might occur along the diagonal length of the box.
    :return: 
    """
    hbl = box_length*half_box_fudge_factor
    q_half_length = 2*math.pi/hbl
    peaks_q = []
    peaks_I = []
    maxima_i = argex(intensities, np.greater)[0]
    for i in maxima_i:
        if qs[i] > q_half_length:
            peaks_q.append(qs[i])
            peaks_I.append(intensities[i])
    return peaks_q, peaks_I


def get_highest_maxima(qs, intensities, box_length):
    """
    Find the first peak excluding peaks at q less than the half box length
    :param qs: wave vectors from scattering
    :param intensities: intensity at wave vector values (q)
    :param box_length: box length
    :return: first peak wave vector (largest_peak_q) and intensity (largest_peak_I)
    """
    peaks_q, peaks_I = get_all_maximas(qs, intensities, box_length)
    if len(peaks_I) > 0:
        largest_peak_I = np.max(peaks_I)
        index_largest_I = peaks_I.index(largest_peak_I)
        largest_peak_q = peaks_q[index_largest_I]
    else:
        largest_peak_q = None
        largest_peak_I = None
    return largest_peak_q, largest_peak_I
