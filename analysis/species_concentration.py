import signac
import gsd
import gsd.fl
import gsd.hoomd
import networkx as nx
import numpy as np
from freud import box, density
import matplotlib.pyplot as plt
import matplotlib
#plt.style.use('ggplot')
#%matplotlib inline
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)
matplotlib.rc('axes', labelsize=20)
matplotlib.rc('lines', linewidth=2)

def get_particle_idxs(ptype,snapshot):
    pidx = []
    for i,idx in enumerate(snapshot.particles.typeid):
        if idx == ptype:
            pidx.append(i)
    return pidx

def get_bond_group_dict(snapshot):
    bond_rank={}
    for i in range(snapshot.particles.N):
        bond_rank[i]=0
    for bidx in snapshot.bonds.group:
        bond_rank[bidx[0]]+=1
        bond_rank[bidx[1]]+=1
    return bond_rank

def get_bond_rank_hist(snapshot,species='amine'):
    if species is 'amine':
        bond_rank={0:0,1:0,2:0,3:0,4:0}
        typeid=0
    bond_group_dic = get_bond_group_dict(snapshot)
    amineidxs = get_particle_idxs(typeid,snapshot)
    for idx in amineidxs:
        prank = bond_group_dic[idx]
        bond_rank[prank]+=1
    return bond_rank

def saveSpeciesConcentrations(job, species='amine', nIntervals=30):
    gsd_path=job.fn('data.gsd')
    f = gsd.fl.GSDFile(gsd_path, 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    n_frames = len(t)
    times=[]
    uns=[]
    ps=[]
    ss=[]
    ts=[]
    qs=[]
    curperct = []
    total_possable = job.sp.num_a*job.sp.n_mul*4
    print('total possible',total_possable)
    print('total frames',n_frames)

    for fid in range(n_frames-1,n_frames,nIntervals):
        print('frame:',fid)
        snapshot = t[fid]
        bond_ranks = get_bond_rank_hist(snapshot,species)
        times.append(fid)
        total_bonds = bond_ranks[1] + bond_ranks[2]*2 + bond_ranks[3]*3 + bond_ranks[4]*4
        print('total bonds',total_bonds)
        curperct.append(total_bonds/total_possable)
        uns.append(bond_ranks[0])
        ps.append(bond_ranks[1])
        ss.append(bond_ranks[2])
        ts.append(bond_ranks[3])
        qs.append(bond_ranks[4])

    time_conv = job.sp.dcd_write#1e3

    fig, ax = plt.subplots()
    ax1 = ax.twinx()
    plt.rcParams.update({'mathtext.default':  'regular' })

    ln1 = ax.plot(np.array(times)*time_conv,uns, label=r"A$_0$")
    ln2 = ax.plot(np.array(times)*time_conv,ps, label=r"A$_1$")
    ln3 = ax.plot(np.array(times)*time_conv,ss, label=r"A$_2$")
    ln4 = ax.plot(np.array(times)*time_conv,ts, label=r"A$_3$")
    ln5 = ax.plot(np.array(times)*time_conv,qs, label=r"A$_4$")
    ln6 = ax1.plot(np.array(times)*time_conv, np.array(curperct)*100, linestyle="--", label="Cure Percent")

    ax1.set_ylabel("Cure Precent")
    ax.set_xlabel("Timestep")
    ax.set_ylabel("Amine Count")
    ax.xaxis.major.formatter._useMathText = True
    ax.yaxis.major.formatter._useMathText = True
    ax.ticklabel_format(style='sci', axis='x', scilimits=(0,0), useMathText=True)
    ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0), useMathText=True)


    lns = ln1 + ln2 + ln3 + ln4 + ln5 + ln6
    labs = [l.get_label() for l in lns]
    ax1.legend(lns, labs, loc="best")
    #plt.tight_layout()
    plt.savefig(job.fn('amine_concentration.png'),transparent=True)
