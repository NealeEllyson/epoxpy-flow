package require pbctools 
puts "=================Rendering final frame of epoxy molecule==================="
set noderank [parallel noderank]
puts "node $noderank is running ..."
set num [molinfo top get numframes]
set frame 0
pbc wrap -center origin
pbc box -color black -center origin -width 6
scale to 10.0
display resetview
rotate x by 15
rotate y by 15
render TachyonInternal vmdscene.tga "convert vmdscene.tga -fuzz 0% -trim -transparent white ./final_snapshot.png"

