import numpy as np

def diffusivity_from_msd(job, start_ts=0, start_from_percent=None):
    dimensions=3
    data = np.genfromtxt(job.fn('msd.log'),names=True)
    time_steps = data['timestep']
    A_msd = data['aparticles']
    B_msd = data['bparticles']
    C_msd = data['cparticles']
    time = time_steps*job.sp.md_dt
    if start_from_percent is not None:
        start_ts = int(len(time)*start_from_percent/100.)
        time = time[start_ts:]
    msd_A = A_msd[start_ts:]
    msd_B = B_msd[start_ts:]
    msd_C = C_msd[start_ts:]
    # determine the slop using linear regression
    par = np.polyfit(time, msd_A, 1, full=True)
    drdt_A = par[0][0]#0-slope, 1-intercept
    par = np.polyfit(time, msd_B, 1, full=True)
    drdt_B = par[0][0]
    par = np.polyfit(time, msd_C, 1, full=True)
    drdt_C = par[0][0]
    #calculate the diffusion coefficient
    D_A = drdt_A/(2*dimensions)
    D_B = drdt_B/(2*dimensions)
    D_C = drdt_C/(2*dimensions)
    return D_A,D_B,D_C
