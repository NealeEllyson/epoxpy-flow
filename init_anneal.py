#!/usr/bin/env python
"""Initialize the project's data space.

Iterates over all defined state points and initializes
the associated job workspace directories."""
import signac
import numpy as np
import epoxpy.temperature_profile_builder as tpb
import itertools
import epoxpy.common as cmn


project = signac.init_project('EpoxpyProject')
quench_time = 6e6 
search_criteria = {'bond':True,"kT":3.0}
print('search criteria:',search_criteria)
cure_jobs = project.find_jobs(search_criteria)
P=10.0
tau_factor = 100
tauP_factor = 1000
start_quenchT=1.3#0.4
final_quenchT=0.3#0.2
quench_interval=0.03

for cure_job in cure_jobs:
    print('annealing from',start_quenchT,'to',final_quenchT)
    qp = tpb.StepwiseQuenchTemperatureProfileBuilder(initial_temperature=start_quenchT,
                                                     final_temperature=final_quenchT,
                                                     quench_time=quench_time,
                                                     first_step_buffer_time=quench_time,
                                                     quench_interval=quench_interval)

    sp = cure_job.statepoint()
    sp['curing_job']=cure_job.get_id()
    sp['curing_job_sp']=cure_job.statepoint()#cure_job.get_id()
    sp['quench_temp_prof']=qp.get_raw()
    sp['quench_T']=round(final_quenchT,2)
    sp['quench_time']=quench_time
    sp['cooling_method']='anneal'
    sp['bond']=False
    sp['mix_time']=0
    sp['integrator']=cmn.Integrators.NPT.name
    sp['P']=P
    sp['tau']=sp['md_dt']*tau_factor#1.0#0.001,0.01,0.1,1.0,10
    sp['tauP']=sp['md_dt']*tauP_factor #1,10,100,1000
    project.open_job(sp).init()
project.write_statepoints()
