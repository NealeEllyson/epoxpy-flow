import signac

# not ave but min :)
kT_to_ave_gel_point = {
    0.8: 21040000.0,
    0.9: 12600000.0,
    1.0: 8400000.0,
    1.1: 6280000.0,
    1.2: 4200000.0,
    1.3: 4200000.0,
}


def calc_quench_kT(start_kT, end_kT=0.8, step=0.1, always_add=None):
    kTs = []
    new_kT = start_kT
    while new_kT > end_kT:
        new_kT -= step
        kTs.append(round(new_kT, 1))
    if always_add:
        kTs.append(always_add)
    return kTs


project = signac.get_project()
jobs_to_delete = []
for job in project:
    jobs_to_delete.append(job)
    if "gel_point" in job.document and job.sp.activation_energy == 2.1:
        for quench_kT in calc_quench_kT(job.sp.kT, always_add=0.5):
            ave_gel_point = kT_to_ave_gel_point[job.sp.kT]
            step_before_ave_gel_point = ave_gel_point - job.sp.dcd_write
            step_between_0_ave_gel_point = (ave_gel_point / 2) - job.sp.dcd_write
            step_between_end_ave_gel_point = (
                ave_gel_point + (job.sp.t_Final - ave_gel_point) / 2
            ) - job.sp.dcd_write
            for quench_time in [
                step_before_ave_gel_point,
                step_between_0_ave_gel_point,
                step_between_end_ave_gel_point,
            ]:
                quench_time = int(quench_time)
                new_sp = job.statepoint()
                # Make new profile
                quench_prof = job.sp.temp_prof[:3]
                quench_prof.append([quench_time, job.sp.kT])
                quench_prof.append([quench_time + 1, quench_kT])
                quench_prof.append([job.sp.t_Final + job.sp.mix_time, quench_kT])
                new_sp["temp_prof"] = quench_prof
                project.open_job(new_sp).init()

project.write_statepoints()

# for job in jobs_to_delete:
#    job.remove()
