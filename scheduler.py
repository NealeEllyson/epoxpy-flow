from flow.manage import Scheduler
from flow.torque import TorqueJob
import tempfile
import subprocess
from flow.manage import ClusterJob, JobStatus
import signac


#def _fetch(user=None):
#    if user is None:
#        user = getpass.getuser()
#    cmd = "qstat -fx -u {user}".format(user=user)
#    try:
#        result = io.BytesIO(subprocess.check_output(cmd.split()))
#    except FileNotFoundError:
#        raise RuntimeError("Torque not available.")
#    tree = ET.parse(source=result)
#    return tree.getroot()


class DummyTorqueJob(ClusterJob):

    def __init__(self, job):
        self.job = job

    def _id(self):
        return self.job.get_id()

    def __str__(self):
        return str(self._id())

    def name(self):
        return self.job.get_id()

    def status(self):
        #job_state = self.node.find('job_state').text
        #if job_state == 'R':
        #    return JobStatus.active
        #if job_state == 'Q':
        #    return JobStatus.queued
        #if job_state == 'C':
        #    return JobStatus.inactive
        #if job_state == 'H':
        #    return JobStatus.held
        return JobStatus.registered


class PBSProScheduler(Scheduler):
    submit_cmd = ['qsub']

    def __init__(self, user=None, **kwargs):
        super(PBSProScheduler, self).__init__(**kwargs)
        self.user = user

    def jobs(self):
        self._prevent_dos()
        project = signac.get_project('.')#EpoxpyProject')
        for job in project:
            yield DummyTorqueJob(job)
        #nodes = _fetch(user=self.user)
        #for node in nodes.findall('Job'):
        #    yield TorqueJob(node)

    def submit(self, script, after=None, pretend=False, hold=False, flags=None, *args, **kwargs):
        if flags is None:
            flags = []
        elif isinstance(flags, str):
            flags = flags.split()

        submit_cmd = self.submit_cmd + flags

        if after is not None:
            submit_cmd.extend(
                ['-W', 'depend="afterok:{}"'.format(after.split('.')[0])])

        if hold:
            submit_cmd += ['-h']

        if pretend:
            print("# Submit command: {}".format(' '.join(submit_cmd)))
            print(script.read())
            print()
        else:
            with tempfile.NamedTemporaryFile() as tmp_submit_script:
                tmp_submit_script.write(script.read().encode('utf-8'))
                tmp_submit_script.flush()
                output = subprocess.check_output(
                    submit_cmd + [tmp_submit_script.name])
            jobsid = output.decode('utf-8').strip()
            return jobsid

    @classmethod
    def is_present(cls):
        try:
            subprocess.check_output(['qsub', '--version'], stderr=subprocess.STDOUT)
        except (IOError, OSError):
            return False
        else:
            return True
