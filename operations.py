"""This module contains the operation functions for this project.
"""


def initialize(job):
    import epoxpy.temperature_profile_builder as tpb
    fig_path = job.fn('temperature_profile.png')
    temp_temperature_profile = tpb.LinearTemperatureProfileBuilder(0)
    if 'curing_job' in job.sp:
        temp_temperature_profile.set_raw(job.sp.quench_temp_prof)
    else:
        temp_temperature_profile.set_raw(job.sp.temp_prof)
    temp_prof = temp_temperature_profile
    print('tempearture profile:{}'.format(temp_prof.get_raw()))
    fig = temp_prof.get_figure()
    fig.savefig(fig_path)

def quench(job):
    "quenches a pre cured sample to the given temperature"
    if 'quench_T' not in job.sp:
        print('!!!!! WARNING !!!!!! Running quench on curing job. Exiting..')
        return
    import hoomd
    import signac
    import math
    project = signac.init_project('EpoxpyProject')
    if hoomd.context.exec_conf is None:
        hoomd.context.initialize('')
        with job:
            with hoomd.context.SimulationContext():
                import epoxpy.abc_type_epoxy_dpd_simulation as es_dpd
                import epoxpy.abc_type_epoxy_dpdlj_simulation as es_dpdlj
                import epoxpy.abc_type_epoxy_dpdfene_simulation as es_dpdfene
                import epoxpy.abc_type_epoxy_lj_harmonic_simulation as es_lj_H
                import epoxpy.temperature_profile_builder as tpb
                import numpy as np

                temp_quench_temperature_profile = tpb.LinearTemperatureProfileBuilder(0)
                temp_quench_temperature_profile.set_raw(job.sp.quench_temp_prof)
                quench_temp_prof = temp_quench_temperature_profile
                cure_jobs = project.find_jobs(job.statepoint()['curing_job_sp'])
                if len(cure_jobs)!=1:
                    for dup_job in cure_jobs:
                        print(dup_job)
                    raise ValueError('curing_job_sp produced more than one job id')
                for curing_job in cure_jobs:
                    ext_init_struct_path = curing_job.fn('final.hoomdxml')
                if job.sp.pot == 'LangH':
                    if 'P' in job.sp:
                        myEpoxySim = es_lj_H.ABCTypeEpoxyLJHarmonicSimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                               mix_kt=job.sp.mix_kt,
                                                               temp_prof=quench_temp_prof, bond=job.sp.bond,
                                                               n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                               shrink_time=job.sp.shrink_time,
                                                               num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                               legacy_bonding=job.sp.legacy_bonding,
                                                               ext_init_struct_path=ext_init_struct_path,
                                                               exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                               log_curing=job.sp.log_curing,
                                                               curing_log_period=job.sp.curing_log_period,
                                                               log_write=job.sp.log_write,
                                                               dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                               mix_dt=job.sp.mix_dt,
                                                               md_dt=job.sp.md_dt,
                                                               density=job.sp.density, bond_period=job.sp.bond_period,
                                                               activation_energy=job.sp.activation_energy,
                                                               sec_bond_weight=job.sp.sec_bond_weight,
                                                               use_dybond_plugin=job.sp.use_dybond_plugin,
                                                               profile_run=job.sp.profile_run,
                                                               nl_tuning=job.sp.nl_tuning,
                                                               stop_after_percent=job.sp.stop_after_percent,
                                                               percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                               AA_interaction=job.sp.AA_interaction,
                                                               BB_interaction=job.sp.BB_interaction,
                                                               CC_interaction=job.sp.CC_interaction,
                                                               AB_interaction=job.sp.AB_interaction,
                                                               AC_interaction=job.sp.AC_interaction,
                                                               BC_interaction=job.sp.BC_interaction,
                                                               AB_bond_const=job.sp.AB_bond_const,
                                                               AB_bond_dist=job.sp.AB_bond_dist,
                                                               CC_bond_const=job.sp.CC_bond_const,
                                                               CC_bond_dist=job.sp.CC_bond_dist,
                                                               CC_bond_angle=job.sp.CC_bond_angle,
                                                               CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                               AA_alpha=job.sp.AA_alpha,
                                                               AB_alpha=job.sp.AB_alpha,
                                                               AC_alpha=job.sp.AC_alpha,
                                                               BC_alpha=job.sp.BC_alpha,
                                                               gamma=job.sp.gamma,
                                                               old_init=job.sp.old_init,
                                                               integrator=job.sp.integrator,
                                                               P=job.sp.P,
                                                               tau=job.sp.tau,
                                                               tauP=job.sp.tauP,
                                                               max_a_bonds=job.sp.max_a_bonds,
                                                               max_b_bonds=job.sp.max_b_bonds)
                    else:
                        myEpoxySim = es_lj_H.ABCTypeEpoxyLJHarmonicSimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                               mix_kt=job.sp.mix_kt,
                                                               temp_prof=quench_temp_prof, bond=job.sp.bond,
                                                               n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                               shrink_time=job.sp.shrink_time,
                                                               num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                               legacy_bonding=job.sp.legacy_bonding,
                                                               ext_init_struct_path=ext_init_struct_path,
                                                               exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                               log_curing=job.sp.log_curing,
                                                               curing_log_period=job.sp.curing_log_period,
                                                               log_write=job.sp.log_write,
                                                               dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                               mix_dt=job.sp.mix_dt,
                                                               md_dt=job.sp.md_dt,
                                                               density=job.sp.density, bond_period=job.sp.bond_period,
                                                               activation_energy=job.sp.activation_energy,
                                                               sec_bond_weight=job.sp.sec_bond_weight,
                                                               use_dybond_plugin=job.sp.use_dybond_plugin,
                                                               profile_run=job.sp.profile_run,
                                                               nl_tuning=job.sp.nl_tuning,
                                                               stop_after_percent=job.sp.stop_after_percent,
                                                               percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                               AA_interaction=job.sp.AA_interaction,
                                                               BB_interaction=job.sp.BB_interaction,
                                                               CC_interaction=job.sp.CC_interaction,
                                                               AB_interaction=job.sp.AB_interaction,
                                                               AC_interaction=job.sp.AC_interaction,
                                                               BC_interaction=job.sp.BC_interaction,
                                                               AB_bond_const=job.sp.AB_bond_const,
                                                               AB_bond_dist=job.sp.AB_bond_dist,
                                                               CC_bond_const=job.sp.CC_bond_const,
                                                               CC_bond_dist=job.sp.CC_bond_dist,
                                                               CC_bond_angle=job.sp.CC_bond_angle,
                                                               CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                               AA_alpha=job.sp.AA_alpha,
                                                               AB_alpha=job.sp.AB_alpha,
                                                               AC_alpha=job.sp.AC_alpha,
                                                               BC_alpha=job.sp.BC_alpha,
                                                               gamma=job.sp.gamma,
                                                               old_init=job.sp.old_init,
                                                               integrator=job.sp.integrator,
                                                               max_a_bonds=job.sp.max_a_bonds,
                                                               max_b_bonds=job.sp.max_b_bonds)
                elif job.sp.pot == 'DPD': 
                    if 'P' in job.sp:
                        myEpoxySim = es_dpd.ABCTypeEpoxyDPDSimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                   mix_kt=job.sp.mix_kt,
                                                   temp_prof=quench_temp_prof, bond=job.sp.bond,
                                                   n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                   shrink_time=job.sp.shrink_time,
                                                   shrinkT=job.sp.shrinkT,
                                                   num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                   legacy_bonding=job.sp.legacy_bonding,
                                                   ext_init_struct_path=ext_init_struct_path,
                                                   exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                   log_curing=job.sp.log_curing,
                                                   curing_log_period=job.sp.curing_log_period,
                                                   log_write=job.sp.log_write,
                                                   dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                   mix_dt=job.sp.mix_dt,
                                                   md_dt=job.sp.md_dt,
                                                   density=job.sp.density, bond_period=job.sp.bond_period,
                                                   activation_energy=job.sp.activation_energy,
                                                   sec_bond_weight=job.sp.sec_bond_weight,
                                                   use_dybond_plugin=job.sp.use_dybond_plugin,
                                                   profile_run=job.sp.profile_run,
                                                   nl_tuning=job.sp.nl_tuning,
                                                   stop_after_percent=job.sp.stop_after_percent,
                                                   percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                   AA_interaction=job.sp.AA_interaction,
                                                   AB_interaction=job.sp.AB_interaction,
                                                   AC_interaction=job.sp.AC_interaction,
                                                   BC_interaction=job.sp.BC_interaction,
                                                   AB_bond_const=job.sp.AB_bond_const,
                                                   AB_bond_dist=job.sp.AB_bond_dist,
                                                   CC_bond_const=job.sp.CC_bond_const,
                                                   CC_bond_dist=job.sp.CC_bond_dist,
                                                   CC_bond_angle=job.sp.CC_bond_angle,
                                                   CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                   gamma=job.sp.gamma,
                                                   old_init=job.sp.old_init,
                                                   integrator=job.sp.integrator,
                                                   P=job.sp.P,
                                                   tau=job.sp.tau,
                                                   tauP=job.sp.tauP,
                                                   max_a_bonds=job.sp.max_a_bonds,
                                                   max_b_bonds=job.sp.max_b_bonds)
                    else:
                        myEpoxySim = es_dpd.ABCTypeEpoxyDPDSimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                   mix_kt=job.sp.mix_kt,
                                                   temp_prof=quench_temp_prof, bond=job.sp.bond,
                                                   n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                   shrink_time=job.sp.shrink_time,
                                                   shrinkT=job.sp.shrinkT,
                                                   num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                   legacy_bonding=job.sp.legacy_bonding,
                                                   ext_init_struct_path=ext_init_struct_path,
                                                   exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                   log_curing=job.sp.log_curing,
                                                   curing_log_period=job.sp.curing_log_period,
                                                   log_write=job.sp.log_write,
                                                   dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                   mix_dt=job.sp.mix_dt,
                                                   md_dt=job.sp.md_dt,
                                                   density=job.sp.density, bond_period=job.sp.bond_period,
                                                   activation_energy=job.sp.activation_energy,
                                                   sec_bond_weight=job.sp.sec_bond_weight,
                                                   use_dybond_plugin=job.sp.use_dybond_plugin,
                                                   profile_run=job.sp.profile_run,
                                                   nl_tuning=job.sp.nl_tuning,
                                                   stop_after_percent=job.sp.stop_after_percent,
                                                   percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                   AA_interaction=job.sp.AA_interaction,
                                                   AB_interaction=job.sp.AB_interaction,
                                                   AC_interaction=job.sp.AC_interaction,
                                                   BC_interaction=job.sp.BC_interaction,
                                                   AB_bond_const=job.sp.AB_bond_const,
                                                   AB_bond_dist=job.sp.AB_bond_dist,
                                                   CC_bond_const=job.sp.CC_bond_const,
                                                   CC_bond_dist=job.sp.CC_bond_dist,
                                                   CC_bond_angle=job.sp.CC_bond_angle,
                                                   CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                   gamma=job.sp.gamma,
                                                   old_init=job.sp.old_init,
                                                   max_a_bonds=job.sp.max_a_bonds,
                                                   max_b_bonds=job.sp.max_b_bonds)
                else:
                    raise ValueError('Unknown simulation requested for quench operation. Check simulation parameters')
                myEpoxySim.execute()

def mix_and_cure(job):
    "Initialize the simulation configuration."
    if 'curing_job' in job.sp:
        print('quenching job',job)
        quench(job)
        return
    import hoomd
    if hoomd.context.exec_conf is None:
        hoomd.context.initialize('')
    with job:
        with hoomd.context.SimulationContext():
            import epoxpy.abc_type_epoxy_dpd_simulation as es_dpd
            import epoxpy.abc_type_epoxy_dpdlj_simulation as es_dpdlj
            import epoxpy.abc_type_epoxy_dpdfene_simulation as es_dpdfene
            import epoxpy.abc_type_epoxy_lj_harmonic_simulation as es_lj_H
            import epoxpy.temperature_profile_builder as tpb
            import numpy as np

            temp_temperature_profile = tpb.LinearTemperatureProfileBuilder(0)
            temp_temperature_profile.set_raw(job.sp.temp_prof)
            temp_prof = temp_temperature_profile
            print('tempearture profile:{}'.format(temp_prof))
            fig_path = job.fn('temperature_profile.png')
            fig = temp_prof.get_figure()
            fig.savefig(fig_path)
            if job.sp.pot == 'DPD':
                myEpoxySim = es_dpd.ABCTypeEpoxyDPDSimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                       mix_kt=job.sp.mix_kt,
                                                       temp_prof=temp_prof, bond=job.sp.bond,
                                                       n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                       shrink_time=job.sp.shrink_time,
                                                       shrinkT=job.sp.shrinkT,
                                                       num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                       legacy_bonding=job.sp.legacy_bonding,
                                                       ext_init_struct_path=job.sp.ext_init_struct_path,
                                                       exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                       log_curing=job.sp.log_curing,
                                                       curing_log_period=job.sp.curing_log_period,
                                                       log_write=job.sp.log_write,
                                                       dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                       mix_dt=job.sp.mix_dt,
                                                       md_dt=job.sp.md_dt,
                                                       density=job.sp.density, bond_period=job.sp.bond_period,
                                                       activation_energy=job.sp.activation_energy,
                                                       sec_bond_weight=job.sp.sec_bond_weight,
                                                       use_dybond_plugin=job.sp.use_dybond_plugin,
                                                       profile_run=job.sp.profile_run,
                                                       nl_tuning=job.sp.nl_tuning,
                                                       stop_after_percent=job.sp.stop_after_percent,
                                                       percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                       AA_interaction=job.sp.AA_interaction,
                                                       AB_interaction=job.sp.AB_interaction,
                                                       AC_interaction=job.sp.AC_interaction,
                                                       BC_interaction=job.sp.BC_interaction,
                                                       AB_bond_const=job.sp.AB_bond_const,
                                                       AB_bond_dist=job.sp.AB_bond_dist,
                                                       CC_bond_const=job.sp.CC_bond_const,
                                                       CC_bond_dist=job.sp.CC_bond_dist,
                                                       CC_bond_angle=job.sp.CC_bond_angle,
                                                       CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                       gamma=job.sp.gamma,
                                                       old_init=job.sp.old_init,
                                                       max_a_bonds=job.sp.max_a_bonds,
                                                       max_b_bonds=job.sp.max_b_bonds)
            if job.sp.pot == 'DPD_FENE':
                myEpoxySim = es_dpdfene.ABCTypeEpoxyDPDFENESimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                       mix_kt=job.sp.mix_kt,
                                                       temp_prof=temp_prof, bond=job.sp.bond,
                                                       n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                       shrink_time=job.sp.shrink_time,
                                                       num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                       legacy_bonding=job.sp.legacy_bonding,
                                                       ext_init_struct_path=job.sp.ext_init_struct_path,
                                                       exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                       log_curing=job.sp.log_curing,
                                                       curing_log_period=job.sp.curing_log_period,
                                                       log_write=job.sp.log_write,
                                                       dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                       mix_dt=job.sp.mix_dt,
                                                       md_dt=job.sp.md_dt,
                                                       density=job.sp.density, bond_period=job.sp.bond_period,
                                                       activation_energy=job.sp.activation_energy,
                                                       sec_bond_weight=job.sp.sec_bond_weight,
                                                       use_dybond_plugin=job.sp.use_dybond_plugin,
                                                       profile_run=job.sp.profile_run,
                                                       nl_tuning=job.sp.nl_tuning,
                                                       stop_after_percent=job.sp.stop_after_percent,
                                                       percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                       AA_interaction=job.sp.AA_interaction,
                                                       AB_interaction=job.sp.AB_interaction,
                                                       AC_interaction=job.sp.AC_interaction,
                                                       BC_interaction=job.sp.BC_interaction,
                                                       AB_bond_const=job.sp.AB_bond_const,
                                                       AB_bond_dist=job.sp.AB_bond_dist,
                                                       CC_bond_const=job.sp.CC_bond_const,
                                                       CC_bond_dist=job.sp.CC_bond_dist,
                                                       CC_bond_angle=job.sp.CC_bond_angle,
                                                       CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                       AA_alpha=job.sp.AA_alpha,
                                                       AB_alpha=job.sp.AB_alpha,
                                                       AC_alpha=job.sp.AC_alpha,
                                                       BC_alpha=job.sp.BC_alpha,
                                                       gamma=job.sp.gamma,
                                                       old_init=job.sp.old_init,
                                                       max_a_bonds=job.sp.max_a_bonds,
                                                       max_b_bonds=job.sp.max_b_bonds)
            elif job.sp.pot == 'LJ':
                myEpoxySim = es_dpdlj.ABCTypeEpoxyDPDLJSimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                       mix_kt=job.sp.mix_kt,
                                                       temp_prof=temp_prof, bond=job.sp.bond,
                                                       n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                       shrink_time=job.sp.shrink_time,
                                                       num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                       legacy_bonding=job.sp.legacy_bonding,
                                                       ext_init_struct_path=job.sp.ext_init_struct_path,
                                                       exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                       log_curing=job.sp.log_curing,
                                                       curing_log_period=job.sp.curing_log_period,
                                                       log_write=job.sp.log_write,
                                                       dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                       mix_dt=job.sp.mix_dt,
                                                       md_dt=job.sp.md_dt,
                                                       density=job.sp.density, bond_period=job.sp.bond_period,
                                                       activation_energy=job.sp.activation_energy,
                                                       sec_bond_weight=job.sp.sec_bond_weight,
                                                       use_dybond_plugin=job.sp.use_dybond_plugin,
                                                       profile_run=job.sp.profile_run,
                                                       nl_tuning=job.sp.nl_tuning,
                                                       stop_after_percent=job.sp.stop_after_percent,
                                                       percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                       AA_interaction=job.sp.AA_interaction,
                                                       AB_interaction=job.sp.AB_interaction,
                                                       AC_interaction=job.sp.AC_interaction,
                                                       BC_interaction=job.sp.BC_interaction,
                                                       AB_bond_const=job.sp.AB_bond_const,
                                                       AB_bond_dist=job.sp.AB_bond_dist,
                                                       CC_bond_const=job.sp.CC_bond_const,
                                                       CC_bond_dist=job.sp.CC_bond_dist,
                                                       CC_bond_angle=job.sp.CC_bond_angle,
                                                       CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                       AA_alpha=job.sp.AA_alpha,
                                                       AB_alpha=job.sp.AB_alpha,
                                                       AC_alpha=job.sp.AC_alpha,
                                                       BC_alpha=job.sp.BC_alpha,
                                                       gamma=job.sp.gamma,
                                                       old_init=job.sp.old_init,
                                                       max_a_bonds=job.sp.max_a_bonds,
                                                       max_b_bonds=job.sp.max_b_bonds)
            elif job.sp.pot == 'LangH':
                myEpoxySim = es_lj_H.ABCTypeEpoxyLJHarmonicSimulation(job.sp.sim_name, mix_time=job.sp.mix_time,
                                                       mix_kt=job.sp.mix_kt,
                                                       temp_prof=temp_prof, bond=job.sp.bond,
                                                       n_mul=job.sp.n_mul, shrink=job.sp.shrink,
                                                       shrink_time=job.sp.shrink_time,
                                                       shrinkT=job.sp.shrinkT,
                                                       num_a=job.sp.num_a,num_b=job.sp.num_b,num_c10=job.sp.num_c10,
                                                       legacy_bonding=job.sp.legacy_bonding,
                                                       ext_init_struct_path=job.sp.ext_init_struct_path,
                                                       exclude_mixing_in_output=job.sp.exclude_mixing_in_output,
                                                       log_curing=job.sp.log_curing,
                                                       curing_log_period=job.sp.curing_log_period,
                                                       log_write=job.sp.log_write,
                                                       dcd_write=job.sp.dcd_write, output_dir=job.workspace(),
                                                       mix_dt=job.sp.mix_dt,
                                                       md_dt=job.sp.md_dt,
                                                       density=job.sp.density, bond_period=job.sp.bond_period,
                                                       activation_energy=job.sp.activation_energy,
                                                       sec_bond_weight=job.sp.sec_bond_weight,
                                                       use_dybond_plugin=job.sp.use_dybond_plugin,
                                                       profile_run=job.sp.profile_run,
                                                       nl_tuning=job.sp.nl_tuning,
                                                       stop_after_percent=job.sp.stop_after_percent,
                                                       percent_bonds_per_step=job.sp.percent_bonds_per_step,
                                                       AA_interaction=job.sp.AA_interaction,
                                                       BB_interaction=job.sp.BB_interaction,
                                                       CC_interaction=job.sp.CC_interaction,
                                                       AB_interaction=job.sp.AB_interaction,
                                                       AC_interaction=job.sp.AC_interaction,
                                                       BC_interaction=job.sp.BC_interaction,
                                                       AB_bond_const=job.sp.AB_bond_const,
                                                       AB_bond_dist=job.sp.AB_bond_dist,
                                                       CC_bond_const=job.sp.CC_bond_const,
                                                       CC_bond_dist=job.sp.CC_bond_dist,
                                                       CC_bond_angle=job.sp.CC_bond_angle,
                                                       CC_bond_angle_const=job.sp.CC_bond_angle_const,
                                                       AA_alpha=job.sp.AA_alpha,
                                                       AB_alpha=job.sp.AB_alpha,
                                                       AC_alpha=job.sp.AC_alpha,
                                                       BC_alpha=job.sp.BC_alpha,
                                                       gamma=job.sp.gamma,
                                                       old_init=job.sp.old_init,
                                                       max_a_bonds=job.sp.max_a_bonds,
                                                       max_b_bonds=job.sp.max_b_bonds)

            myEpoxySim.execute()
            log_path = job.fn('curing.log')
            np.savetxt(log_path, myEpoxySim.curing_log)

def post_process(job):
    import analysis.diffusivity as diffusivity
    import analysis.rdf as rdf
    import analysis.species_concentration as concentration
    import analysis.structure_factor as sf
    import analysis.gel_point as gel
    import analysis.cure_fit as cure_fit
    import numpy as np
    import subprocess as sp
    from sys import platform
    import gsd
    import gsd.fl
    import gsd.hoomd
    import analysis.log_utils as log_utils
    import analysis.network_analysis as network_analysis
    import signac
    from shutil import copyfile 
    project = signac.init_project('EpoxpyProject')

    with job:
        if job.isfile('final.hoomdxml') and job.isfile('final_snapshot.png')==False:
            if platform == "linux" or platform == "linux2":
                print('rendering on Linux',job)
                sp.call('vmd -hoomd final.hoomdxml -dispdev text -eofexit <../../analysis/render_final_frame_Linux.tcl',shell=True)
            elif platform == "darwin":
                sp.call('rlwrap /Applications/VMD 1.9.3.app/Contents/MacOS/startup.command -hoomd final.hoomdxml -dispdev text -eofexit <../../analysis/render_final_frame_MAC.tcl',shell=True)
            else:
                print('++++++++ WARNING: Cannot determine platform!++++++++')
        if job.sp.bond is True:
            data = np.genfromtxt(job.fn('out.log'),names=True)
            cure_percent = data['bond_percentAB'][-1]
            print('cure percent:',cure_percent)
            job.document['cure_percent']=cure_percent
            cure_fit.saveCureFits(job)
            gel.save_gel_point(job)
            print('Saved cluster analysis for calculating gelation plot')
        if 'curing_job_sp' in job.sp:
            cure_jobs = project.find_jobs(job.statepoint()['curing_job_sp'])
            if len(cure_jobs)!=1:
                for dup_job in cure_jobs:
                    print(dup_job)
                raise ValueError('curing_job_sp produced more than one job id:',len(cure_jobs))
            for curing_job in cure_jobs:
                if 'cure_percent' in curing_job.document:
                    print('############saving cure_percent ###############')
                    job.document['cure_percent']=curing_job.document['cure_percent']

        if job.isfile('data.gsd'):
            f = gsd.fl.GSDFile(job.fn('data.gsd'), 'rb')
            t = gsd.hoomd.HOOMDTrajectory(f)
            n_frames = len(t)
            end_frame = n_frames-1
            snapshot = t[end_frame]
            sim_box = snapshot.configuration.box
            job.document['Lx'] = np.float64(sim_box[0])#JSON serializer does not like float32
            job.document['Ly'] = np.float64(sim_box[1])
            job.document['Lz'] = np.float64(sim_box[2])
        if job.isfile('out.log'):
            job.document['volume'],_ = log_utils.get_mean_and_std(job,'volume')
            job.document['temperature'],_ = log_utils.get_mean_and_std(job,'temperature')
            job.document['pressure'],_ = log_utils.get_mean_and_std(job,'pressure')
        if job.sp.bond:
            if job.isfile('data.gsd') and 'network_size_see' not in job.document:
                network_analysis.save_network_data(job)
        if job.isfile('msd.log'):
            D_A,D_B,D_C = diffusivity.diffusivity_from_msd(job, start_ts=0, start_from_percent=30.)
            job.document['D_A'] = D_A
            job.document['D_B'] = D_B
            job.document['D_C'] = D_C
            print('Calculated diffusivity')

        if 'curing_job' not in job.sp:
        #    concentration.saveSpeciesConcentrations(job,species='amine',nIntervals=10)
        #    print('Saved species concentration for amines')
        #    gel.save_gel_point(job)
        #    print('Saved cluster analysis for calculating gelation plot')
        #    print('Saved species concentration for amines')
        #    if job.sp.n_mul <= 1e3:
        #        rdf.saveRDFs(job, nFrames=10, dr=0.01)
        #        print('Calculated RDFs and saved the figures')
            if not job.isfile('diffract_type_{}/asq.txt'.format(2)):
                sf.save_structure_factor(job)
            if not job.isfile('asq.png') and job.isfile('diffract_type_{}/asq.png'.format(2)):
                copyfile(job.fn('diffract_type_{}/asq.png'.format(2)),job.fn('asq.png'))
            sf.save_peaks_and_time_evo(job)
        #    print('Saved structure factor data for the tougheners')

if __name__ == '__main__':
    import flow
    flow.run()
