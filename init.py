#!/usr/bin/env python
"""Initialize the project's data space.

Iterates over all defined state points and initializes
the associated job workspace directories."""
import signac
import numpy as np
import epoxpy.temperature_profile_builder as tpb
import itertools
import epoxpy.common as cmn


def diff_statepoints(a,b):
                nSp_a = len(a)
                nSp_b = len(b)
                if nSp_a==nSp_b:
                    for key in a:
                        if b[key] != a[key]:
                            yield key,a[key],b[key]
                elif nSp_a>nSp_b:
                    for key in a:
                        if key in b:
                            if b[key] != a[key]:
                                yield key,a[key],b[key]
                        else:
                            yield key,a[key],b,None
                else:
                    for key in b:
                        if key in a:
                            if b[key] != a[key]:
                                yield key,a[key],b[key]
                        else:
                            yield key,None,b[key]

def getBeadVolDensityAndMass(i,j,T):
    Na = 6.022e23
    frac_DDS = 10000/50000
    frac_DEGBA = 20000/50000
    frac_PES10 = (10*2000)/50000

    M_DDS   = 248.306
    M_DEGBA = 340.423
    M_PES10 = 2324.614/10 #g/mol

    density_eqs = {'DDS':(-0.00069,1.52),'DEGBA':(-0.00081,1.37),'PES10':(-0.0003,1.39)}
    rho_DDS   = (density_eqs['DDS'][0]*T) + density_eqs['DDS'][1]
    rho_DEGBA = (density_eqs['DEGBA'][0]*T) + density_eqs['DEGBA'][1]
    rho_PES10 = (density_eqs['PES10'][0]*T) + density_eqs['PES10'][1]

    av_M = M_DDS*frac_DDS + M_DEGBA*frac_DEGBA + M_PES10*frac_PES10

    av_rho = (rho_DDS*frac_DDS) + (rho_DEGBA*frac_DEGBA) + (rho_PES10*frac_PES10)
    cm_cubed_toA_cubed = 1e24
    av_vol = cm_cubed_toA_cubed*av_M/av_rho/Na #A^3

    #print('av_vol',av_vol,'A^3')
    return av_vol,av_rho,av_M

def kai(i,j,T):
    kB = 1.381e-23 #JK^-1
    cm_cubed_toA_cubed = 1e24
    Vbead,dummy,dummy = getBeadVolDensityAndMass(i,j,T)
    Vbead_SI=Vbead*1e-30

    solubility_eqs = {'DEGBA': (-20.9678, 26788.6657), 'PES10': (-6.7178, 21993.1474), 'DDS': (-19.2091, 30848.6453)}
    delta_i_SI = (solubility_eqs[i][0]*T) + solubility_eqs[i][1]
    delta_j_SI = (solubility_eqs[j][0]*T) + solubility_eqs[j][1]
    kai = Vbead_SI/(kB*T) * (delta_i_SI-delta_j_SI)**2
    return kai

def aij(T,i,j):
    aii=25.0#Groot and Warren
    kai_ij=kai(i,j,T)
    delta_a = kai_ij/0.286
    return aii+delta_a

def E_scale(T):
    Na = 6.022e23
    kB = 1.381e-23 #JK^-1
    return kB*T*Na

production=True
project = signac.init_project('EpoxpyProject')
if production:
    n_mul = 1000.0
    curing_log_period = 1e5
    mixing_time = 5e3
else:
    n_mul = 2.0
    curing_log_period = 1e4
    mixing_time = 1e2

blend=False #True#False
if blend:
    num_a=10
    num_b=20
    num_c10=2
    drink='blend'
    N=50000
else:
    num_a=1
    num_b=2
    num_c10=0
    drink='neat'
    N=50001

av_calibrationT = 439.3636#K
av_aij_DDS_DDS = 25.0
av_aij_DDS_DEGBA = 30.729
av_aij_DDS_PES = 25.003
av_aij_DEGBA_PES = 30.532

temperature_profiles =  ['iso']#,'lin_ramp']#,'step']
bonding_periods = [10]#[1,5,10,20,40,80,100]
percent_bonds_per_steps = [0.005]#[0.0025,.005,0.01,1]#,100] # in number for nmul=1000: [1,2,4,400,40000]
freq_factors=[]
for bp in bonding_periods:
    for pbps in percent_bonds_per_steps:
        freq_factors.append((pbps,bp))
#freq_factors = [(0.005,10.0)]#[(0.0025,10.0),(0.005,10.0),(0.01,10.0),(0.1,100.0)]#[0.1,0.2,0.4]
kTs =[1.7]#[0.5,0.7,1.0,1.3,1.7]#[1.7]#[1.0,1.7]#[0.5,1.0,1.7]##,1,3]#3,5]#np.asarray([200,300,400,500,600,700])/av_calibrationT#np.asarray([200,425,600,850,1000])/av_calibrationT
print('kTs being simulated:',kTs)
log_periods = [1e4]#[1,1e1,1e2,1e3,1e4]
calibrationTs=[av_calibrationT]#[200,425,600,850,1000]
stop_after_percents = np.arange(0,110,10,dtype=float)#[10.,40.,70.,100.]#[90.0]#95.0]#99.0,75.0
activation_energies =[2.0]#[2.0,1.0]# [2.0,5.0,10]
secondary_bond_weights = [1.2]
mixing_temperature = 4.0
bonding_methods = ['dybond']#,'freud','legacy']
statepoints_init = []
bonding_conditions=[True]#,False]
t_Finals = [6e6]#,1e7,1e8]#1e6,6e6]
t_SetT = 0.75e6
nTrials = 1
E_factors=[1.00]#[1.0,0.25]#0.5,0.25]
gammas =[4.5]#[0.5,2.0,4.5]#[1.0,2.0,3.0,4.0,4.5]#[0.5,4.5]#,18.0,36.0,72.0]# [4.5]#[4.5,9,18,36]
max_a_bonds = 4
max_b_bonds = 2
n_muls = np.asarray([N])/(num_a+num_b+(num_c10*10))
potentials = ['LangH']#['DPD','LJ','DPD_FENE','LangH']
densities=[1.0]#,1.1,1.2]#[0.8,1.0]#3.0]#[1.0,0.8]
tau_factors = [100]
tauP_factors = [1000]
Ps = [6.0]#[2.0, 5.0, 7.5]
cooling_method = 'quench'#'anneal' 'quench'
quenchTime=5e6#1e5#1e6
job_type='cure'#'cure'#'quench'
unlike_interactions = [0.98]#np.arange(0.8,1.0,0.03,dtype=float)
quench_integrator = cmn.Integrators.NVT.name

params =[temperature_profiles,#0
         freq_factors,#1
         kTs,#2
         log_periods,#3
         calibrationTs,#4
         stop_after_percents,#5
         activation_energies,#6
         secondary_bond_weights,#7
         bonding_methods,#8
         bonding_conditions,#9
         t_Finals,#10
         E_factors,#11
         gammas,#12
         n_muls,#13
         potentials,#14
         densities,#15
         range(nTrials),#16
         tau_factors,#17
         tauP_factors,#18
         Ps,#19
         unlike_interactions]#20
param_combinations=list(itertools.product(*params))
for params in param_combinations:
    temperature_profile=params[0]
    freq_factor=params[1]
    kT=params[2]
    log_period=params[3]
    calibrationT=params[4]
    stop_after_percent=params[5]
    act_energy=params[6]
    sec_bond_weight=params[7]
    bonding_method=params[8]
    bonding_condition=params[9]
    t_Final=params[10]
    E_factor=params[11]
    gamma=params[12]
    n_mul=params[13]
    potential=params[14]
    density=params[15]
    trial=params[16]
    tau_factor=params[17]
    tauP_factor=params[18]
    P=params[19]
    unlike_interaction=params[20]

    if bonding_conditions is False and temperature_profile is not 'iso':
        continue
    if temperature_profile is 'iso':
        temp_profile = tpb.LinearTemperatureProfileBuilder(initial_temperature=kT)
                                                                #initial_time=mixing_time)
        temp_profile.add_state_point(t_Final, kT)
    elif temperature_profile is 'lin_ramp':
        temp_profile = tpb.LinearTemperatureProfileBuilder(initial_temperature=300/calibrationT,
                                                                initial_time=mixing_time)
        temp_profile.add_state_point(t_Final, kT)
    elif temperature_profile is 'step':
        temp_profile = tpb.LinearTemperatureProfileBuilder(initial_temperature=300/calibrationT,
                                                                initial_time=mixing_time)
        temp_profile.add_state_point(t_SetT, kT)
        temp_profile.add_state_point(t_Final, kT)

    if bonding_method == 'dybond':
        use_dybond = True
        legacy = False
    elif bonding_method == 'freud':
        use_dybond = False
        legacy = False
    elif bonding_method == 'legacy':
        use_dybond = False
        legacy = True

    if potential == 'DPD':
        AA_energy = av_aij_DDS_DDS
        AB_energy = av_aij_DDS_DEGBA
        AC_energy = av_aij_DDS_PES
        BC_energy = av_aij_DEGBA_PES
        AB_bond_dist=0.0
        CC_bond_dist=0.0
        AB_bond_const=4.0
        CC_bond_const=4.0
        AA_alpha=None
        AB_alpha=None
        AC_alpha=None
        BC_alpha=None
        shrink_time=1e3
        mix_dt=1e-2
        md_dt=1e-2
        this_E_factor=1.0
        this_density=3.0
    elif potential == 'LJ':
        this_E_factor=E_factor
        AA_energy = 1.0*this_E_factor
        AB_energy = 0.2*this_E_factor
        AC_energy = 0.2*this_E_factor
        BC_energy = 0.2*this_E_factor
        AB_bond_dist=1.0
        CC_bond_dist=1.0
        AB_bond_const=100.0
        CC_bond_const=100.0
        AA_alpha=1.0
        AB_alpha=1.0
        AC_alpha=1.0
        BC_alpha=1.0
        shrink_time=1e6
        mix_dt=1e-4
        md_dt=1e-2
        this_density=density
    elif potential == 'LangH':
        this_E_factor=E_factor
        AA_energy = 1.0*this_E_factor
        BB_energy = 0.90*this_E_factor
        CC_energy = 0.98*this_E_factor
        AB_energy = (AA_energy*BB_energy)**(1/2) 
        AC_energy = (AA_energy*CC_energy)**(1/2)
        BC_energy = (CC_energy*BB_energy)**(1/2)
        AB_bond_dist=1.0
        CC_bond_dist=1.0
        AB_bond_const=100.0
        CC_bond_const=100.0
        AA_alpha=1.0
        AB_alpha=1.0
        AC_alpha=1.0
        BC_alpha=1.0
        shrink_time=1e5
        mix_dt=1e-4
        md_dt=1e-2
        this_density=density
    elif potential == 'DPD_FENE':
        this_E_factor=E_factor
        AA_energy = 1.0*this_E_factor
        AB_energy = 0.2*this_E_factor
        AC_energy = 0.2*this_E_factor
        BC_energy = 0.2*this_E_factor
        AB_bond_dist=1.0
        CC_bond_dist=1.0
        AB_bond_const=30.0
        CC_bond_const=30.0
        AA_alpha=1.0
        AB_alpha=1.0
        AC_alpha=1.0
        BC_alpha=1.0
        shrink_time=1e6
        mix_dt=1e-4
        md_dt=1e-2
        this_density=density

    sp = {'sim_name': '{}_{}_{}kT'.format(drink,temperature_profile,kT),#
          'kT':kT,#only for flat temperature profiles
          'T':kT*calibrationT,
          't_Final':t_Final,
          't_SetT':t_SetT,
          'profile':temperature_profile,
          'T':kT*calibrationT,
          'n_particles':n_mul*(num_a+num_b+(num_c10*10)),
          'calibrationT':calibrationT,
          'E_scale':E_scale(calibrationT),
          'mix_time': mixing_time,
          'mix_kt': mixing_temperature,
          'temp_prof': temp_profile.get_raw(),
          'bond': bonding_condition,
          'n_mul': n_mul,
          'num_a': num_a,
          'num_b': num_b,
          'num_c10': num_c10,
          'shrink': True,
          'shrink_time':shrink_time,
          'shrinkT':mixing_temperature,
          'use_dybond_plugin':use_dybond,
          'legacy_bonding': legacy,
          'ext_init_struct_path': None,
          'exclude_mixing_in_output': True,#False,
          'log_curing': True,
          'curing_log_period': curing_log_period,
          'log_write': log_period,#5e3,
          'dcd_write': 2e4,
          'bond_period': freq_factor[1],#bonding_period,
          'mix_dt': 1e-4,
          'md_dt': 1e-2,
          'density': this_density,#1.0,
          'activation_energy': act_energy,
          'sec_bond_weight': sec_bond_weight,
          'profile_run': False,
          'nl_tuning': False,
          'stop_after_percent':stop_after_percent,
          'percent_bonds_per_step':freq_factor[0],#percent_bonds_per_step,
          'trial':trial,
          'AA_interaction':AA_energy,
          'BB_interaction':BB_energy,
          'CC_interaction':CC_energy,
          'AB_interaction':AB_energy,
          'AC_interaction':AC_energy,
          'BC_interaction':BC_energy,
          'E_factor':this_E_factor,
          'AB_bond_const':AB_bond_const,
          'AB_bond_dist': AB_bond_dist,
          'CC_bond_const':CC_bond_const,
          'CC_bond_dist': CC_bond_dist,
          'AA_alpha':AA_alpha,
          'AB_alpha':AB_alpha,
          'AC_alpha':AC_alpha,
          'BC_alpha':BC_alpha,
          'gamma':gamma,
          'pot':potential,
          'old_init':False,
          'max_a_bonds':max_a_bonds,
          'max_b_bonds':max_b_bonds}
    if job_type=='quench':
        cure_jobs = project.find_jobs(sp)
        if len(cure_jobs)==1:
            for cure_job in cure_jobs:
                use_curing_job_P = False
                start_quenchT=0.5
                final_quenchT=0.1
                quench_interval=0.01
                if cooling_method == 'quench':
                    quenchTs=np.arange(start_quenchT,final_quenchT+quench_interval,-quench_interval)
                    print('Quenching starts at',start_quenchT,'and ends at',final_quenchT)
                    print('There will be',len(quenchTs),'quench points')
                    print('quenchTs:',quenchTs)
                    for quenchT in quenchTs:
                        qp = tpb.LinearTemperatureProfileBuilder(initial_temperature=quenchT)
                        #print('Quenching',cure_job,'to',quenchT)
                        qp.add_state_point(quenchTime,quenchT)
                        sp['cooling_method']='quench'
                        sp['curing_job']=cure_job.get_id()
                        sp['curing_job_sp']=cure_job.statepoint()#cure_job.get_id()
                        sp['quench_temp_prof']=qp.get_raw()
                        sp['quench_T']=round(quenchT,2)
                        sp['quench_time']=quenchTime
                        #sp['ext_init_struct_path']=init_file
                        sp['bond']=False
                        sp['mix_time']=0
                        sp['integrator']=quench_integrator
                        sp['use_curing_job_P']=use_curing_job_P
                        sp['P']=P
                        sp['tau']=sp['md_dt']*tau_factor
                        sp['tauP']=sp['md_dt']*tauP_factor
                        project.open_job(sp).init()
                elif cooling_method == 'anneal':
                    qp = tpb.StepwiseQuenchTemperatureProfileBuilder(initial_temperature=start_quenchT,
                                                                     final_temperature=final_quenchT,
                                                                     quench_time=quenchTime,
                                                                     first_step_buffer_time=quenchTime,
                                                                     quench_interval=quench_interval)

                    sp['curing_job']=cure_job.get_id()
                    sp['curing_job_sp']=cure_job.statepoint()#cure_job.get_id()
                    sp['quench_temp_prof']=qp.get_raw()
                    sp['quench_T']=round(final_quenchT,2)
                    sp['quench_time']=quenchTime
                    sp['cooling_method']='anneal'
                    sp['bond']=False
                    sp['mix_time']=0
                    sp['integrator']=quench_integrator
                    sp['use_curing_job_P']=use_curing_job_P
                    sp['P']=P
                    sp['tau']=sp['md_dt']*tau_factor#1.0#0.001,0.01,0.1,1.0,10
                    sp['tauP']=sp['md_dt']*tauP_factor #1,10,100,1000
                    project.open_job(sp).init()
        else:
            if len(cure_jobs)>1:
                for dup_job in cure_jobs:
                    print('dup_job:',dup_job)
            else:
                print('did not find a cure_job for sp:',sp)
            expected_curing_job = project.open_job(id='1c7d20a2d1d1baef6f94e3b560781d87')
            diff_keys = diff_statepoints(sp,expected_curing_job.statepoint())
            for key,v1,v2 in diff_keys:
                print(key,'\n','j1',':',v1,'\n','j2',':',v2)
            print('!!!!!! WARNING !!!!!!')
            print('Running quench for statepoint that does not contain a cured morphology. Run curing first.')
            print('Found',len(cure_jobs),'jobs')
            print(sp)
    else:
        project.open_job(sp).init()
project.write_statepoints()


