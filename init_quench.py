#!/usr/bin/env python
"""Initialize the project's data space.

Iterates over all defined state points and initializes
the associated job workspace directories."""
import signac
import numpy as np
import epoxpy.temperature_profile_builder as tpb
import itertools
import epoxpy.common as cmn


project = signac.init_project('EpoxpyProject')
quench_time = 1e7 
search_criteria = {"bond":True,"kT":3.0}
print('search criteria:',search_criteria)
cure_jobs = project.find_jobs(search_criteria)
P=10.0
tau_factor = 100
tauP_factor = 1000
start_quenchT=1.0#0.4
final_quenchT=0.6#0.2
quench_interval=0.01#0.1

for cure_job in cure_jobs:
   quenchTs=np.arange(start_quenchT,final_quenchT-quench_interval,-quench_interval)
   #quenchTs=[5.0, 4.0, 3.0, 2.0, 1.0, 0.8, 0.6, 0.50, 0.40, 0.30]#0.2,0.3,0.4,0.5]#just to run quenching a long time
   print('Quenching starts at',start_quenchT,'and ends at',final_quenchT)
   print('There will be',len(quenchTs),'quench points')
   print('quenchTs:',quenchTs)
   for quenchT in quenchTs:
       qp = tpb.LinearTemperatureProfileBuilder(initial_temperature=quenchT)
       #print('Quenching',cure_job,'to',quenchT)
       qp.add_state_point(quench_time,quenchT)
       sp=cure_job.statepoint()
       sp['cooling_method']='quench'
       sp['curing_job']=cure_job.get_id()
       sp['curing_job_sp']=cure_job.statepoint()#cure_job.get_id()
       sp['quench_temp_prof']=qp.get_raw()
       sp['quench_T']=round(quenchT,2)
       sp['quench_time']=quench_time
       sp['bond']=False
       sp['mix_time']=0
       sp['integrator']=cmn.Integrators.NPT.name
       sp['P']=P
       sp['tau']=sp['md_dt']*tau_factor
       sp['tauP']=sp['md_dt']*tauP_factor
       project.open_job(sp).init()
project.write_statepoints()
