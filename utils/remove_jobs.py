import signac
import gsd
import gsd.fl
import gsd.hoomd

project = signac.get_project('../')
yes_to_all=False
cancel=False
n_deleted_jobs=0
pretend_to_delete_all=False
for job in project:
    if 'integrator' in job.sp and job.sp.integrator == 'NPT' and job.sp.P == 0.0:
        if pretend_to_delete_all:
            n_deleted_jobs+=1
            print("deleting job",job,n_deleted_jobs)
        elif yes_to_all:
            n_deleted_jobs+=1
            print("deleting job",job,n_deleted_jobs)
            job.remove()
        else:
            print(job.sp.sim_name)
            resp = input("Delete " + job.get_id()+" ?" + "(y(yes), n(no), a(yes to all), c(cancel):, p(pretend to delete all) ")
            if resp is 'p':
                pretend_to_delete_all=True
                n_deleted_jobs+=1
                print("deleting job",job,n_deleted_jobs)
            elif resp is 'y':
                n_deleted_jobs+=1
                print("deleting job",job,n_deleted_jobs)
                job.remove()
            elif resp is 'a':
                yes_to_all=True
                n_deleted_jobs+=1
                print("deleting job",job,n_deleted_jobs)
                job.remove()
            elif resp is 'c':
                cancel=True
        if cancel:
            break
       
print('deleted',n_deleted_jobs,'jobs')
