import gsd
import gsd.fl
import gsd.hoomd
import signac
import pandas as pd
import sys

def save_frame(job,frame):
    with gsd.hoomd.open(job.fn('Frame{}.gsd'.format(frame)), 'wb') as t_new:
        f = gsd.fl.GSDFile(job.fn('data.gsd'), 'rb')
        t = gsd.hoomd.HOOMDTrajectory(f)
        snap = t[frame]
        t_new.append(snap)

if __name__ == '__main__':
    project = signac.get_project('../')
    df_index = pd.DataFrame(project.index())
    df_index = df_index.set_index(['_id'])
    statepoints = {doc['_id']: doc['statepoint'] for doc in project.index()}
    df = pd.DataFrame(statepoints).T.join(df_index)
    df = df.sort_values('T')
    
    typeId=2
    n_views=40
    grid_size=512
    df_filtered = df[(df.n_particles==500000)& \
                     (df.dcd_write==1000)& \
                     (df.gamma ==36)]
    for signac_id in df_filtered['signac_id']:
        frames = [9,27,45,63,81,99,117,135,153,171,189]
        for frame in frames:
            job = project.open_job(id=signac_id)
            print(job.workspace())
            save_frame(job,frame)#123)
